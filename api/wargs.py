import os
import time

from webargs import fields

userpass = {
    "username": fields.Str(required=True), 
    "password": fields.Str(required=True)
}

logintoken = {
    "files-jwt": fields.Str(required=True)
}

adduser = {
    "username": fields.Str(required=True), 
    "password": fields.Str(required=True), 
    "admin": fields.Bool(missing=False)
}

deleteuser = {
    "ids": fields.List(fields.Int(required=True), required=True)
}

uploadfile = {
    "file": fields.Field(required=True)
}

fileoptions = {
    "ttl": fields.Int()
}

fileurl = {
    "id": fields.Int(required=True),
    "filename": fields.Str()
}

deletefile = {
    "ids": fields.List(fields.Int(required=True), required=True)
}
