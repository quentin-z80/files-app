function gologin() {
    window.location.href = "/login.html"
}

async function logout() {

    const response = await fetch("/api/logout");
    if (response.status == 200) {
        window.location.href = "/login.html";
    } else {
        console.log(response);
    }
}

function leaveindex() {
    if (pagename == "index") {
        window.location.href = "/about.html"
    }
}

async function islogin() {

    const logbut = document.getElementById("login-but");

    const response = await fetch("/api/login");
    const parsedJson = await response.json();

    if (response.status == 403 || response.status == 422) {
        leaveindex();
        logbut.innerText = "Login";
        logbut.onclick = gologin;
        document.getElementById("upload-but").onclick = "return false";
    } else {
        logbut.innerText = "Logout";
        logbut.onclick = logout;
    }

}

const pagename = document.currentScript.getAttribute("page");

islogin();

//document.getElementById("home-but").onclick = homebut

//document.getElementById("upload-but").onclick = uploadbut

//document.getElementById("about-but").onclick = aboutbut

