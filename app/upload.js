var DEFAULT_TTL = 172800; //48 hours

async function uploadfiles() {

    let filesbtn = document.getElementById("files");
    let ttlinput = document.getElementById("ttl-input");

    if (filesbtn.files.length < 1) {
        return;
    }

    let data = new FormData();
    for (const x of filesbtn.files) {
        data.append('file', x);
    }
    
    if (ttlinput.value == "") {
        data.ttl = DEFAULT_TTL + Math.floor(Date.now() / 1000);
    } else if (ttlinput.value == "0") {
        data.ttl = 0;
    } else {
        data.append("ttl", Math.floor((ttlinput.value * 3600) + (Date.now() / 1000)));
    }

    const response = await fetch("/api/files", {
        method: 'POST',
        body: data
    });

    const parsedJson = await response.json();
    console.log(parsedJson);

    if (response.status == 201) {
        window.location.href = "/index.html";
    }

}

document.getElementById('upload-btn').onclick = uploadfiles;
